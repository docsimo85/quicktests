import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ServiziotestService} from "./serviziotest.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  templateUrl: './internal-comp.component.html',
  styleUrls: ['./internal-comp.component.css']
})

export class InternalCompComponent implements OnInit {

  @Input() rating: number | undefined
  @Input() rating2: number | undefined
  prodotti: any;
  datoPassato: any;

  @Output() ratingClicked: EventEmitter<string> =
    new EventEmitter<string>()

  constructor(private service: ServiziotestService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  onclick(): void {
    this.ratingClicked.emit(`the rating ${this.rating} was clicked`);
    console.log(this.prodotti, 'dato passato:' + this.datoPassato)
  }

  ngOnInit(): void {
    this.prodotti = this.service.getProduct();
    this.datoPassato = this.route.snapshot.params["id"];
  }

  back(): void {
    this.router.navigate(['/']);
  }

}
