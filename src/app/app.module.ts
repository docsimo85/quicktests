import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {InternalCompComponent} from './internal-comp/internal-comp.component';
import {RouterModule} from "@angular/router";
import {TestguardGuard} from "./testguard.guard";
import {NotFoundComponent} from './errorPage/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    InternalCompComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '404', component: NotFoundComponent},
      {
        path: 'stica/:id',
        canActivate: [TestguardGuard],
        canDeactivate: [TestguardGuard],
        component: InternalCompComponent
      },
      {path: 'stica', component: InternalCompComponent},
      {path: '**', redirectTo: '/', pathMatch: 'full'}

    ])
  ],
  exports: [InternalCompComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
