import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanDeactivate, Router, RouterStateSnapshot,} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TestguardGuard implements CanActivate, CanDeactivate<unknown> {
  constructor(private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const id = route.params["id"];
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/404']);
      return false;
    }
    return true
  }

  canDeactivate(): boolean {
    return true
  }

}
