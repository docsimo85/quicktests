import {TestBed} from '@angular/core/testing';

import {ServiziotestService} from './serviziotest.service';

describe('ServiziotestService', () => {
  let service: ServiziotestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiziotestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
