import {ComponentFixture, TestBed} from '@angular/core/testing';

import {InternalCompComponent} from './internal-comp.component';

describe('InternalCompComponent', () => {
  let component: InternalCompComponent;
  let fixture: ComponentFixture<InternalCompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InternalCompComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
